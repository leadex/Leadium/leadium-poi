package org.leadium.poi.junit5

import org.apache.commons.lang3.tuple.Pair
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ArgumentsSource
import java.io.File

@DisplayName("Example")
class Example {

    companion object {

        const val spreadsheetId = "1ezi0MqvZhYPs0IiX0_jIBkotJPAs3eEa2uFVT-z2Q5Q"
        const val envSystemPropertyKey = "envTestDataDir"
        const val envPlaceholder = "env"
        const val sheetName = "debug-$envPlaceholder"
        const val testCaseReferenceKey = "testCaseReference"
        const val testNameColumn = "testCaseReference"
        const val testDescriptionColumn = "testDescription"
    }

    @ParameterizedTest(name = "{1}")
    @ArgumentsSource(SheetsProvider::class)
    @SheetsProvider.ReferenceAsDisplayName
    @SheetsProvider.Settings(
        resourcesPath = "/src/apps/fin-to/resources/finto",
        xlsxName = "bae-finto-api.xlsx",
        envSystemPropertyKey = envSystemPropertyKey,
        sheetName = "debug-qa",
        testNameColumn = testNameColumn,
        numLinesToSkip = 1
    )
    fun test(
        rowIndex: Int,
        testName: String,
        testData: LinkedHashMap<String, Pair<String, String>>,
        testDataFile: File
    ) {
        val testCaseClass = Class.forName(testData[testCaseReferenceKey]!!.left)
//        val testCase = testCaseClass.getDeclaredConstructor(testData.javaClass).newInstance(testData) as TestCase
//        DescriptionAppender(System.getProperty("rootProjectPath"), System.getProperty("gitHubMasterUrl"))
//            .appendTestCaseDescription(testData[testDescriptionColumn]!!.left)
//            .appendLinkToTestCase(testCase)
//            .appendLinkToFile(testDataFile, "Test Data")
//            .appendTestDataMap(testData)
//            .apply()
//        testCase.begin()
    }
}