package org.leadium.poi.junit5

import org.apache.commons.lang3.tuple.Pair
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.security.GeneralSecurityException
import java.util.logging.Logger
import java.util.stream.Stream

class SheetsProvider : ArgumentsProvider {

    private val log = Logger.getLogger(this.javaClass.simpleName)

    @Target(AnnotationTarget.FUNCTION)
    annotation class ReferenceAsDisplayName

    @Target(AnnotationTarget.FUNCTION)
    annotation class Settings(
        val resourcesPath: String,
        val xlsxName: String,
        val envSystemPropertyKey: String,
        val sheetName: String,
        val testNameColumn: String,
        val numLinesToSkip: Int = 0
    )

    @Throws(IOException::class, GeneralSecurityException::class)
    override fun provideArguments(context: ExtensionContext): Stream<out Arguments> {
        val referenceAsDisplayName = context.testMethod.get().isAnnotationPresent(ReferenceAsDisplayName::class.java)
        val settings = context.testMethod.get().getAnnotation(Settings::class.java)
        val rootProjectPath = System.getProperty("rootProjectPath")
        val envTestDataDir = System.getProperty(settings.envSystemPropertyKey)
        val xlsxFile = File("$rootProjectPath/${settings.resourcesPath}/$envTestDataDir/${settings.xlsxName}")
        val fis = FileInputStream(xlsxFile)
        val workbook = XSSFWorkbook(fis)
        val spreadsheet: XSSFSheet = workbook.getSheet(settings.sheetName)
        val rowIterator: Iterator<Row> = spreadsheet.iterator()
        val data = readData(rowIterator)
        fis.close()
        val arguments = ArrayList<Arguments>()
        if (data.isEmpty()) {
            log.warning("No data found.")
        } else {
            data.forEachIndexed { rowIndex, row ->
                val rowData = LinkedHashMap<Any, Pair<Any, Any>>()
                row.forEachIndexed { columnIndex, _ ->
                    rowData.addRow(data, row, columnIndex)
                }
                if (rowIndex >= settings.numLinesToSkip) {
                    var testName = rowData[settings.testNameColumn]!!.left as String
                    if (referenceAsDisplayName) testName = testName.toDisplayName()
                    val map = rowData as LinkedHashMap<String, Pair<String, String>>
                    arguments.add(Arguments.of(rowIndex, testName, map, xlsxFile))
                }
            }
        }
        return arguments.stream()
    }

    private fun readData(rowIterator: Iterator<Row>): ArrayList<ArrayList<Any>> {
        val data = ArrayList<ArrayList<Any>>()
        var lastColumn = 0
        while (rowIterator.hasNext()) {
            val row = rowIterator.withIndex().next().value
            if (row.lastCellNum.toInt() > lastColumn)
                lastColumn = row.lastCellNum.toInt()
            val cellData = ArrayList<Any>()
            for (index in 0 until lastColumn) {
                val cell = row.getCell(index, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK)
                cellData.add(cell.stringCellValue)
            }
            if (rowIsEmpty(cellData)) break
            data.add(cellData)
        }
        return data
    }

    private fun rowIsEmpty(cellData: ArrayList<Any>): Boolean {
        return cellData[0].toString().isBlank()
    }

    private fun String.toDisplayName(): String {
        val sb = StringBuilder()
        this.replaceCamelCaseWithUnderscores()
            .replace("_", " ")
            .split(".")
            .reversed()
            .forEach { s: String -> sb.append(s).append(" | ") }
        return sb.toString().substring(0, sb.toString().length - 3)
    }

    private fun String.replaceCamelCaseWithUnderscores() =
        this.replace(Regex("([A-Z])([A-Z])([a-z])|([a-z])([A-Z])"), "\$1\$4_\$2\$3\$5")

    private fun HashMap<Any, Pair<Any, Any>>.addRow(
        values: List<List<Any>>,
        row: List<Any>,
        columnIndex: Int
    ): HashMap<Any, Pair<Any, Any>> {
        val map = this
        map[values[0][columnIndex]] = Pair.of(row[columnIndex], Any())
        return map
    }
}